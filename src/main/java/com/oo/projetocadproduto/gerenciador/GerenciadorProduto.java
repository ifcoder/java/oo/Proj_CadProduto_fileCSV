package com.oo.projetocadproduto.gerenciador;

import com.oo.projetocadproduto.classes.Produto;
import com.oo.projetocadproduto.file.FilePersistence;
import com.oo.projetocadproduto.file.SerializadorJSONProduto;
import com.oo.projetocadproduto.file.SerializadorXMLProduto;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Produtos")
public class GerenciadorProduto {
    
    @XmlElement(name = "Produto")
    private List<Produto> produtos;

    public GerenciadorProduto() {
        this.produtos = new ArrayList<>();
    }

    public void adicionarProduto(Produto produto) {
        produtos.add(produto);
        System.out.println("Produto adicionado com sucesso!");
    }

    public boolean removerProduto(String cod) {
        for (Produto produto : produtos) {
            if (produto.getCod().equals(cod)) {
                produtos.remove(produto);
                System.out.println("Produto removido com sucesso!");
                return true;
            }
        }
        System.out.println("Produto não encontrado!");
        return false;
    }

    public Produto buscarProduto(String cod) {
        for (Produto produto : produtos) {
            if (produto.getCod().equals(cod)) {
                return produto;
            }
        }
        return null;
    }

    public void atualizarProduto(String codAntigo, Produto produtoNovo) {
        Produto produtoExistente = buscarProduto(codAntigo);
        
        if (produtoExistente != null) {
            int indice = produtos.indexOf(produtoExistente);
            produtos.set(indice, produtoNovo);
            System.out.println("Produto atualizado com sucesso.");
        } else {
            System.out.println("Produto com o nome " + codAntigo + " não encontrado.");
        }
    }

    @Override
    public String toString() {
        StringBuilder saida = new StringBuilder();
        for (Produto produto : produtos) {
            saida.append(produto.toString()).append("\n");
        }
        return saida.toString();
    }

    public void salvarNoArquivo(String caminhoDoArquivo) {
        //Serializando XML
        SerializadorJSONProduto serializadorJSON = new SerializadorJSONProduto();
        String jsonData = serializadorJSON.ToJSON(this.produtos);
        
        FilePersistence filePersistence = new FilePersistence();
        filePersistence.saveToFile(jsonData, caminhoDoArquivo);
        System.out.println("Produtos salvos com sucesso em " + caminhoDoArquivo);
    }

    public void carregarDoArquivo(String caminhoDoArquivo) {
        FilePersistence filePersistence = new FilePersistence();
        String jsonData = filePersistence.loadFromFile(caminhoDoArquivo);

        //Desserializando JSON
        SerializadorJSONProduto desserializadorJSON = new SerializadorJSONProduto();
        this.produtos = desserializadorJSON.fromJSON(jsonData);

        System.out.println("Produtos carregados com sucesso de " + caminhoDoArquivo);
    }

}
