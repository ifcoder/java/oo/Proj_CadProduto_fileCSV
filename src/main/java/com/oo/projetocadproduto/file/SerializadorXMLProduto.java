package com.oo.projetocadproduto.file;

import com.oo.projetocadproduto.classes.Produto;
import com.oo.projetocadproduto.gerenciador.GerenciadorProduto;
import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author jose
 */
public class SerializadorXMLProduto {
    // Serializa um objeto Filme para XML como String
    public String ToXML(GerenciadorProduto produtos) {
        try {
            JAXBContext context = JAXBContext.newInstance(GerenciadorProduto.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            StringWriter writer = new StringWriter();
            marshaller.marshal(produtos, writer);
            return writer.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // Desserializa XML em formato String para um objeto Filme
    public GerenciadorProduto fromXML(String xmlString) {
        try {
            JAXBContext context = JAXBContext.newInstance(GerenciadorProduto.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            StringReader reader = new StringReader(xmlString);

            return (GerenciadorProduto) unmarshaller.unmarshal(reader);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
