# ProjCadProduto (Serialização)
- Cadastro de Produto para exemplificar a serialização
- Projeto completinho
	- FilePersistence
	- SerializadorCSVProduto
	- GerenciadorProduto
	- Com interface gráfica (GUI - Swing)


![Imagem](./src/main/resources/images/cadProduto.png)


## Atenção
- Quando for criar seu arquivo .xml ou .json, não se esqueça de preenche-los com algum valor inicial
- Caso você não coloque algum valor inicial ao iniciar sua tela de cadastro será chamado o metodo de load do arquivo e como ele estará vazio, dará algum erro.
